#! /bin/bash

RED='\033[0;31m'
NC='\033[0m' # No Color

if source ./env/bin/activate; then
    echo "Virtualenv found"
else
    virtualenv -p python3 env
    source env/bin/activate
fi
pip install -r requirements.txt

#wget https://gitlab.com/space-kerala/lekha-OCR-database/repository/archive.zip?ref=master
